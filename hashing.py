import random
import string
import hashlib
import hmac





#Returns a random word
def make_word():
    return ''.join(random.choice(string.letters) for x in xrange(5))


#return a hash  password with a name, pw and word
def make_pw_hash(name, pw, word = None):
    if not word:
        word = make_word()
    h = hashlib.sha256(name + pw + word).hexdigest()
    return '%s,%s' % (h, word)

#validate a password
def valid_pw(name, pw, hash):
    word = hash.split(',')[1] #gets the random word
    return hash == make_pw_hash(name, pw, word)

def hash_cookie(username, secretword):
	return hmac.new(str(username + secretword)).hexdigest()


