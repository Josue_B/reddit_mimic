array = list(string.ascii_lowercase)



def rot13_character(string):
	if string:
		if string.islower():
			return array[(array.index(string)+13)%26].lower()
		elif string.isupper():
			string  = string.lower()
			return array[(array.index(string)+13)%26].upper()
		elif string == " ":
			return string
		else:
			return cgi.escape(string)


class Rot13Homework(webapp2.RequestHandler):
	def write_text(self, textie=""):
		if textie:
			return rot13html % {'textie' : textie}
		return rot13html % {'textie' : ""}


	def get(self):
		html = self.write_text("")
		self.response.write(html)

	def post(self):
		text = self.request.get("text")
		output = ""
		for x in text:
			changed = rot13_character(x)
			output = output + changed


		html = self.write_text(output)
		self.response.write(html)