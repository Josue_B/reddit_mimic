class ThanksHandler(webapp2.RequestHandler):
	def make_url(self, name):
		if name:
			return url % {'query' : name}

	def make_div(self, url):
		if url:
			return div % {'url' : url}

	def get(self):
		name = self.request.get("q")
		self.response.write("<h1>Thanks, " + name + " </h1>")
		name_url = self.make_url(name)

		result = urlfetch.fetch(name_url)
		if result.status_code == 200:
			arr = json.loads(result.content)
			# self.response.write(arr)
			for x in arr['data']:
				embeded = x['embed_url'] 
				self.response.write(self.make_div(embeded[5:]))
