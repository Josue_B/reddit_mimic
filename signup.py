#class SignUp(webapp2.RequestHandler):



	USER_RE = re.compile("^[a-zA-Z0-9_-]{3,20}$")
	PASSWORD_REGEX = re.compile("^.{3,20}$")
	EMAIL_REGEX = re.compile("^[\S]+@[\S]+\.[\S]+$")


	# def valid_username(username):
	# 	return USER_RE.match(username)

	# def valid_password(password):
	# 	return PASSWORD_REGEX.match(password)

	# def valid_email(email):
	# 	return not email or EMAIL_REGEX.match(email)

	# def write_page(self, username="", user_error="", password_error="", match_error="",email_error="", email=""):
	# 	return signup_page %{'username': username, 'user_error': user_error,
	# 												'password_error': password_error,
	# 												'match_error': match_error,
	# 												'email_error': email_error,
	# 												'email': email}


	def get(self):
		self.response.write(self.write_page("", "", "", "", ""))

	def post(self):
		username = self.request.get("username")
		password = self.request.get("password")
		verify = self.request.get("verify")
		email = self.request.get("email")

		valid_user = valid_username(username)
		validated_password = valid_password(password)
		validated_email = valid_email(email)

		error_user = ""
		error_password = ""
		error_match = ""
		error_email = ""


		if valid_user and valid_password and validated_email:
			if (password == verify):
				self.redirect("/welcome?" + "username=" + username )
			else:
				error_password = "Passwords did not match"

		if not valid_user:
			error_user = "That's not a valid username"
		if not validated_password:
			error_password = "That's not a valid password"
		if email:
			if not validated_email:
				error_email = "That's not a valid email"

		# self.response.write(self.write_page(username, error_user, error_password, error_match, error_email, email))
