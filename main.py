import os
import re
from string import letters
import webapp2
import jinja2
from google.appengine.ext import db
import hashing
from webapp2_extras import json


template_dir = os.path.join(os.path.dirname(__file__), 'views')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir),
							   autoescape = True)

secret_word = "8vffuzaHJ5rxDEd99mrK5eTU"
def render_str(template, **params):
	t = jinja_env.get_template(template)
	return t.render(params)
def validate_cookie(string):
	try:
		user_name = string.split("|")[0]
		client_cookie_hashed = str(user_name + "|" + hashing.hash_cookie(user_name, secret_word))
		return string == client_cookie_hashed
	except:
		return False
def make_json(j):
		json = {
			"content": j.content,
			"created": str(j.created),
			"last_modified": str(j.last_modified),
			"subject": j.title
		}
		return json


"""Classes"""
class BaseHandler(webapp2.RequestHandler):
	def render(self, template, **kw):
		self.response.out.write(render_str(template, **kw))

	def write(self, *a, **kw):
		self.response.out.write(*a, **kw)

class SignUp(BaseHandler):
	USER_RE = re.compile("^[a-zA-Z0-9_-]{3,20}$")
	PASSWORD_REGEX = re.compile("^.{3,20}$")
	EMAIL_REGEX = re.compile("^[\S]+@[\S]+\.[\S]+$")

	def user_exist(self, username):
		exist = False
		query = User.all()
		for q in query:
			if q.username == username:
				exist = True
		return exist


	def valid_username(self, username):
		return self.USER_RE.match(username) 

	def valid_password(self, password):
		return self.PASSWORD_REGEX.match(password)

	def valid_email(self, email):
		return not email or self.EMAIL_REGEX.match(email)


	def get(self):
		self.render("signup.html")

	def post(self):
		username = self.request.get("username")
		password = self.request.get("password")
		verify = self.request.get("verify")
		email = self.request.get("email")

		valid_user = self.valid_username(username)
		user_exist = self.user_exist(username)
		validated_password = self.valid_password(password)
		validated_email = self.valid_email(email)

		error_user = ""
		error_password = ""
		error_match = ""
		error_email = ""

		if valid_user and validated_password and validated_email and not user_exist:
			if (password == verify):
				hashed_password = hashing.make_pw_hash(username, password)
				user = User(username=username, password=hashed_password, email=email)
				user.put()
				hashed_cookie = username + "|" + hashing.hash_cookie(username, secret_word)
				self.response.headers.add_header('Set-Cookie', 'name=%s; Path=/blog/welcome' %str(hashed_cookie))
				self.redirect("/blog/welcome")
				
			else:
				error_password = "Passwords did not match"

		if user_exist:
			error_user = "user already exists"
		if not valid_user:
			error_user = "That's not a valid username/ or it has been taken"
		if not validated_password:
			error_password = "That's not a valid password"
		if email:
			if not validated_email:
				error_email = "That's not a valid email"

		self.render("signup.html", username=username, user_error=error_user,
									password_error=error_password, email=email,
									email_error=error_email)

class LogIn(BaseHandler):
	def get(self):
		self.render("login.html")

	def post(self):
		username = self.request.get("username")
		password = self.request.get("password")

		query = User.all()
		for q in query:
			if q.username == username and hashing.valid_pw(username, password, q.password):
				hashed_cookie = username + "|" + hashing.hash_cookie(username, secret_word)
				self.response.headers.add_header('Set-Cookie', 'name=%s; Path=/welcome' %str(hashed_cookie))
				self.redirect("/blog/welcome")

		self.render("login.html", username=username, user_error="Invalid login")

class BlogPost(db.Model):
	title = db.StringProperty(required = True)
	content = db.TextProperty(required = True)
	created = db.DateTimeProperty(auto_now_add = True)
	last_modified = db.DateTimeProperty(auto_now_add = True)

class User(db.Model):
	username = db.StringProperty(required = True)
	password = db.TextProperty(required = True)
	email = db.TextProperty()

class Welcome(BaseHandler):
	def get(self):
		cookie_from_client = self.request.cookies.get("name")
		if validate_cookie(cookie_from_client):
			username = cookie_from_client.split("|")[0]
			self.render("blogfront.html", name=username)
		else:
			self.redirect("/blog/signup")

class WelcomeBlog(BaseHandler):
	

	def get(self):
		q = BlogPost.all()
		if self.request.url.endswith('.json'):
			list = [make_json(each) for each in q]
			self.response.write(json.encode(list))
			self.response.content_type = 'application/json'
		else:
			self.render("blogfront.html", q=q)

class BlogNewPost(BaseHandler):
	def render_html(self, title="", content="", error=""):
		self.render("blogform.html", title=title, content=content, error=error)


	def get(self):
		self.render_html()

	def post(self):
		subject = self.request.get("subject")
		content = self.request.get("content")

		if subject and content:
			post = BlogPost(title=subject, content=content)
			post.put()
			self.redirect("/blog/" + str(post.key().id())) 
		else:
			error = "Both Fields must be filled"
			self.render_html(subject, content, error)

class LogOut(BaseHandler):
	def get(self):
		self.response.delete_cookie('bad_cookie')
		self.redirect('signup')

class BlogById(BaseHandler):
	def get(self, blog_id):
		id = int(blog_id)
		p = BlogPost.get_by_id(id)
		if self.request.url.endswith('.json'): 
			self.response.write(json.encode(make_json(p)))
			self.response.content_type = 'application/json'
		else:
			self.render("blogposted.html", title=p.title, content=p.content)


app = webapp2.WSGIApplication([('/blog/(?:\.json)?', WelcomeBlog), 
	('/blog/newpost', BlogNewPost), ('/blog/(\d+)(?:\.json)?', BlogById),
	('/blog/signup', SignUp), ('/blog/welcome', Welcome), ('/blog/login', LogIn),
	('/blog/logout', LogOut)], debug=True)
